package com.example.aboutme.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DashboardViewContent extends ViewModel{
    private MutableLiveData<String> mText;
    public DashboardViewContent(){
        mText = new MutableLiveData<>();
        mText.setValue(("Nama\t\t: Rifqi Yuner\nUmur\t\t: 20 Tahun\nLahir\t\t\t: 29 Agustus 2000\nHobby\t:Makan & Tidur"));
    }
    public LiveData<String> getText() {
        return mText;
    }
}







